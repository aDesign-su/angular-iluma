import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss']
})
export class DevicesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    window.scrollTo(0, 0); 
  }

  baseUrl: string = '../../../assets'
  sticks: any = [
    {
      id: 1,
      name: 'IQOS Iluma standart',
      discrtiption: 'Самая консервативная модель в линейке. Она сохранила тот же дизайн, что и у IQOS DUO, зато начинка и функционал – такие же как у Iqos Iluma Prime',
      img: `${this.baseUrl}/1.jpg`,
    },
    {
      id: 2,
      name: 'IQOS Iluma Prime',
      discrtiption: 'Преимущества IQOS Iluma Prime по сравнению с другими моделями IQOS больше не будет неприятной проблемы, когда лезвие плохо входит в стик, так как лезвия отсутствует.',
      img: `${this.baseUrl}/2.jpg`,
    },
    {
      id: 3,
      name: 'IQOS ILUMA ONE',
      discrtiption: 'Уникальная система нагрева табака, которая удивит вас своей простотой и функциональностью. ',
      img: `${this.baseUrl}/3.jpg`,
    },
  ]

  setId?: number;

  isShowDetails(id: number) {
    this.setId = id 
    if(id) { 
      this.router.navigate(['/details', id]); 
    } 
  }
}
