import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0); 
  }

  aboutParagraph1: string = 'Добро пожаловать в наш магазин, ваш источник стиков Terea для IQOS! У нас вы найдете разнообразие продукции, включая популярные стики Terea и устройства, такие как iqos Iluma и iluma prime. Мы гордимся предлагаемым ассортиментом, который доступен не только в Москве, но и по всей России.'
  aboutParagraph2: string = 'Благодаря удобной доставке вы можете наслаждаться вашими любимыми стиками Terea в кратчайшие сроки. Не упустите шанс заказать качественные стики Terea прямо сейчас и получить удовольствие от вашего IQOS уже сегодня!'

}
