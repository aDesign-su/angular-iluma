import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KzComponent } from './kz.component';

describe('KzComponent', () => {
  let component: KzComponent;
  let fixture: ComponentFixture<KzComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KzComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
