import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-id',
  templateUrl: './id.component.html',
  styleUrls: ['./id.component.scss']
})
export class IdComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

  baseUrl: string = '../../../assets'
  id: any = [
    {
      id: 1,
      name: 'Green',
      discrtiption: 'Имеет освежающий вкус, с нотами мяты и лимона. Также может быть замечен легкий оттенок дыма и некоторая сладость.',
      img: `${this.baseUrl}/i1.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Индонезия'
    },
    {
      id: 2,
      name: 'Black green',
      discrtiption: 'Ощущение ментола, с мягкой табачной смесью и нотками зеленых фруктов.',
      img: `${this.baseUrl}/i2.jpg`,
      price: '7 500₽ блок',
      info: 'Стики имеют капсулу!',
      region: 'Производство: Индонезия'
    },
    {
      id: 3,
      name: 'Blue',
      discrtiption: 'Обладает некоторой сладостью с нотами фруктов, ванили, карамели и немного ментола.',
      img: `${this.baseUrl}/i3.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Индонезия'
    },
    {
      id: 4,
      name: 'Dimensions Yugen',
      discrtiption: 'Изысканный табачный бленд, дополненный комбинацией фруктовых нот и цветочных ароматов с завершающим свежим аккордом.',
      img: `${this.baseUrl}/i4.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Индонезия'
    },
  ]
}
