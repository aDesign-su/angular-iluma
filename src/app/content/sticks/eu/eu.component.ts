import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eu',
  templateUrl: './eu.component.html',
  styleUrls: ['./eu.component.scss']
})
export class EuComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

  baseUrl: string = '../../../assets'
  eu: any = [
    {
      id: 1,
      name: 'Warm fuse',
      discrtiption: 'Сочетание легкого табака с тонкой смесью тропических фруктов без ментола.',
      img: `${this.baseUrl}/e1.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 2,
      name: 'Amelia pearl',
      discrtiption: 'Сбалансированная смесь обжаренных табаков придает легкий пикантный травяной аромат. Один щелчок освобождает дополнительные освежающие и экзотические ноты.',
      img: `${this.baseUrl}/e2.jpg`,
      price: '7 500₽ блок',
      info: 'Стики имеют капсулу!',
      region: 'Производство: Европа'
    },
    {
      id: 3,
      name: 'Abora pearl',
      discrtiption: 'Вкус характеризуется лёгким и приятным ментолом, а также цитрусовым шлейфом с несколькими травяными оттенками.',
      img: `${this.baseUrl}/e3.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 4,
      name: 'Russet',
      discrtiption: 'Самый крепкий вкус. Богатая смесь обжаренных табаков, с приятными солодовыми ароматическими нотками.',
      img: `${this.baseUrl}/e4.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 5,
      name: 'Briza pearl',
      discrtiption: 'Обжаренный табачный бленд с древесными нотами и ароматом чая, одним нажатием вы можете добавить освежающий ментол.',
      img: `${this.baseUrl}/e5.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 6,
      name: 'Soft Fuse',
      discrtiption: 'Сочетание легкого табака с сливочным послевкусием',
      img: `${this.baseUrl}/e6.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
  ]

}
