import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-am',
  templateUrl: './am.component.html',
  styleUrls: ['./am.component.scss']
})
export class AmComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

  baseUrl: string = '../../../assets'

  am: any = [
    {
      id: 1,
      name: 'Yellow',
      discrtiption: 'Имеет свежий и легкий вкус, с легкими фруктовыми нотками. Он может быть идеальным выбором для тех, кто предпочитает более легкий вкус табака. Кроме того, некоторые пользователи отмечают легкие пряные оттенки, которые могут добавлять некоторую сложность и глубину вкусу.',
      img: `${this.baseUrl}/a1.jpg`,
      price: '4 900₽ блок',
      info: '',
      region: 'Производство: Армения'
    },
    {
      id: 2,
      name: 'Sienna',
      discrtiption: 'Хорошо сбалансированная смесь обжаренного табака с древесными нотками и тонким ароматом чая.',
      img: `${this.baseUrl}/a2.jpg`,
      price: '4 900₽ блок',
      info: '',
      region: 'Производство: Армения'
    },
  ]
}
