/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SticksComponent } from './sticks.component';

describe('SticksComponent', () => {
  let component: SticksComponent;
  let fixture: ComponentFixture<SticksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SticksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SticksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
