import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sticks',
  templateUrl: './sticks.component.html',
  styleUrls: ['./sticks.component.scss']
})
export class SticksComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0); 
  }

  baseUrl: string = '../../../assets'
  eu: any = [
    {
      id: 1,
      name: 'Warm fuse',
      discrtiption: 'Сочетание легкого табака с тонкой смесью тропических фруктов без ментола.',
      img: `${this.baseUrl}/e1.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 2,
      name: 'Amelia pearl',
      discrtiption: 'Сбалансированная смесь обжаренных табаков придает легкий пикантный травяной аромат. Один щелчок освобождает дополнительные освежающие и экзотические ноты.',
      img: `${this.baseUrl}/e2.jpg`,
      price: '7 900₽ блок',
      info: 'Стики имеют капсулу!',
      region: 'Производство: Европа'
    },
    {
      id: 3,
      name: 'Abora pearl',
      discrtiption: 'Вкус характеризуется лёгким и приятным ментолом, а также цитрусовым шлейфом с несколькими травяными оттенками.',
      img: `${this.baseUrl}/e3.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 4,
      name: 'Russet',
      discrtiption: 'Самый крепкий вкус. Богатая смесь обжаренных табаков, с приятными солодовыми ароматическими нотками.',
      img: `${this.baseUrl}/e4.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 5,
      name: 'Briza pearl',
      discrtiption: 'Обжаренный табачный бленд с древесными нотами и ароматом чая, одним нажатием вы можете добавить освежающий ментол.',
      img: `${this.baseUrl}/e5.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 6,
      name: 'Soft Fuse',
      discrtiption: 'Сочетание легкого табака с сливочным послевкусием',
      img: `${this.baseUrl}/e6.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
  ]

  id: any = [
    {
      id: 1,
      name: 'Green',
      discrtiption: 'Имеет освежающий вкус, с нотами мяты и лимона. Также может быть замечен легкий оттенок дыма и некоторая сладость.',
      img: `${this.baseUrl}/i1.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Индонезия'
    },
    {
      id: 2,
      name: 'Black green',
      discrtiption: 'Ощущение ментола, с мягкой табачной смесью и нотками зеленых фруктов.',
      img: `${this.baseUrl}/i2.jpg`,
      price: '7 500₽ блок',
      info: 'Стики имеют капсулу!',
      region: 'Производство: Индонезия'
    },
    {
      id: 3,
      name: 'Blue',
      discrtiption: 'Обладает некоторой сладостью с нотами фруктов, ванили, карамели и немного ментола.',
      img: `${this.baseUrl}/i3.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Индонезия'
    },
    {
      id: 4,
      name: 'Dimensions Yugen',
      discrtiption: 'Изысканный табачный бленд, дополненный комбинацией фруктовых нот и цветочных ароматов с завершающим свежим аккордом.',
      img: `${this.baseUrl}/i4.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Индонезия'
    },
  ]
  am: any = [
    {
      id: 1,
      name: 'Yellow',
      discrtiption: 'Имеет свежий и легкий вкус, с легкими фруктовыми нотками. Он может быть идеальным выбором для тех, кто предпочитает более легкий вкус табака. Кроме того, некоторые пользователи отмечают легкие пряные оттенки, которые могут добавлять некоторую сложность и глубину вкусу.',
      img: `${this.baseUrl}/a1.jpg`,
      price: '4 900₽ блок',
      info: '',
      region: 'Производство: Армения'
    },
    {
      id: 2,
      name: 'Sienna',
      discrtiption: 'Хорошо сбалансированная смесь обжаренного табака с древесными нотками и тонким ароматом чая.',
      img: `${this.baseUrl}/a2.jpg`,
      price: '4 900₽ блок',
      info: '',
      region: 'Производство: Армения'
    },
  ]
  kz: any = [
    {
      id: 1,
      name: 'Turquoise',
      discrtiption: 'Сочетание охлаждающих ментоловых ноток, ярко выраженное среди мощной табачной основы. Вкус отличается бодрящим всплеском свежести.',
      img: `${this.baseUrl}/k1.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 2,
      name: 'Silver*',
      discrtiption: 'Имеет более мягкий и гладкий вкус с оттенками свежести и легкой сладости. Он может быть идеальным выбором для тех, кто ищет более мягкий вкус табака. Кроме того, некоторые пользователи описывают его как немного пряный, с легким ароматом трав и цветов.',
      img: `${this.baseUrl}/k2.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 3,
      name: 'Bronze',
      discrtiption: 'Изысканный вкус отборного табачного бленда, в ароматической симфонии которого присутствуют нотки какао с деликатным отголоском сухофруктов, звучащих в унисон с терпкостью табачного листа.',
      img: `${this.baseUrl}/k3.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 4,
      name: 'Purple Wave',
      discrtiption: 'Охлаждающая и ароматная табачная смесь для любителей ментола и лесных ягод.',
      img: `${this.baseUrl}/k4.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 5,
      name: 'Summer Wave',
      discrtiption: 'Стики, в которых сочетаются изысканный аромат экзотических фруктов и прохладная свежесть мяты. Этот уникальный вкус предоставляет неповторимый опыт, объединяя сладость экзотических фруктов с освежающими нотками мяты.',
      img: `${this.baseUrl}/k5.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 6,
      name: 'Amber',
      discrtiption: 'Terea Amber описывается как насыщенный и глубокий, с легкой сладостью и пряными нотками. Он обладает терпким вкусом, который напоминает аромат традиционного табака, но при этом менее интенсивен и более мягок.',
      img: `${this.baseUrl}/k6.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 7,
      name: 'Ruby Fuse TEREA',
      discrtiption: 'Aроматный табачный вкус с нотками красных ягод и цветочными оттенками без ментола в яркой малиновой упаковке',
      img: `${this.baseUrl}/k7.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 8,
      name: 'Green Zing',
      discrtiption: 'Если вы ищете что-то новое и необычное в мире табачных смесей, то цитрусовый и свежий табачный бленд - это именно то, что вам нужно! Этот уникальный продукт обладает удивительным сочетанием освежающего ментола, цитрусовых нот и тонких травяных оттенков.',
      img: `${this.baseUrl}/k8.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
  ]
}
