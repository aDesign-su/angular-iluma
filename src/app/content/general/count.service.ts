import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CountService {
  constructor() { }

  itemCount!: number;
  items:any = [];

  getItemCount(): number {
    const countString = localStorage.getItem('itemCount');
    return countString ? parseInt(countString, 10) : 0;
  }
  incrementItemCount() {
    let count = this.getItemCount();
    count++;
    localStorage.setItem('itemCount', count.toString());
    return count;
  }
  getCard() {
    const keys = Object.keys(localStorage);
    // Фильтруем только те ключи, которые начинаются с "item_"
    const itemKeys = keys.filter(key => key.startsWith('item_'));

    // Создаем массив для хранения извлеченных данных
    itemKeys.forEach(key => {
      const itemData:any = localStorage.getItem(key);
      // Парсим данные из строки JSON
      const parsedItem = JSON.parse(itemData);
      // Добавляем извлеченные данные в массив
      this.items.push(parsedItem);
    });
    console.log(this.items);
  }
  clearItemCount() {
    const keys = Object.keys(localStorage);
    // Фильтруем только те ключи, которые начинаются с "item_"
    const itemKeys = keys.filter(key => key.startsWith('item_'));
    // Проходимся по каждому ключу и удаляем соответствующие значения из localStorage
    itemKeys.forEach(key => {
      localStorage.removeItem(key);
    });
    localStorage.removeItem('itemCount');
    // Сбросить значение счётчика в компоненте
    this.itemCount = 0;
    this.items = []; 
  }
  clearItemById(id: string) {
    localStorage.removeItem('item_' + id);
    let count = this.getItemCount();
    count--;
    localStorage.setItem('itemCount', count.toString());
    // Обновляем значение счетчика в компоненте
    this.itemCount = count;
    this.items = []; 
    this.getCard();
  }
  
}
