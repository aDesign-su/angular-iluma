import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountService } from '../count.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  constructor(private activeteRoute:ActivatedRoute, public count:CountService) { 
    this.getId = this.activeteRoute.snapshot.params['id']
    this.itemCount = this.count.getItemCount();
  }

  ngOnInit() {
    this.dataSource()
    window.scrollTo(0, 0); 
  }

  getId?: number | null
  data: any

  baseUrl: string = '../../../assets'
  getDataSource: any
  itemCount: number;

  setItem(id: number,price: string,name: string) {
    let item: any = {
      id: id,
      name: name,
      price: price,
    }
    localStorage.setItem('item_' + id, JSON.stringify(item))
  }
  incrementCounter() {
    this.itemCount = this.count.incrementItemCount();
  }

  dataSource() {
    if (this.getId) {
      this.getDataSource = this.assort.find((element:any) => element.id == this.getId);
      if (this.getDataSource) {
          // console.log(this.getDataSource);
      } else {
          console.log("Элемент не найден");
      }
    }
  }

  assort: any = [
    {
      id: 1,
      name: 'IQOS Iluma standart',
      discrtiption: 'Самая консервативная модель в линейке. Она сохранила тот же дизайн, что и у IQOS DUO, зато начинка и функционал – такие же как у Iqos Iluma Prime',
      img: `${this.baseUrl}/1.jpg`,
      price: '9 900 ₽',
      color: 'серый, бежевый, розовый, зелёный, голубой,'
    },
    {
      id: 2,
      name: 'IQOS Iluma Prime',
      discrtiption: 'Преимущества IQOS Iluma Prime по сравнению с другими моделями IQOS больше не будет неприятной проблемы, когда лезвие плохо входит в стик, так как лезвия отсутствует. Не нужно заменять лезвие, ведь его, как вы уже догадались, — нет! В прошлое постепенно уходят палочки, щеточки и прочие принадлежности для чистки — чистка не требуется. Дается 6 минут или 14 затяжек на один сеанс (на одну минуту больше, чем в прошлых моделях). Наконец, IQOS Iluma Prime сделан из алюминия и является премиальным устройством сегмента.',
      img: `${this.baseUrl}/2.jpg`,
      price: '15 000 ₽',
      color: 'золотистый'
    },
    {
      id: 3,
      name: 'IQOS ILUMA ONE',
      discrtiption: 'Уникальная система нагрева табака, которая удивит вас своей простотой и функциональностью. Это идеальное решение для тех, кто хочет получить максимум удовольствия от 4-го поколения устройств и не переплачивать, по сути имеет тот же функционал что и вся линейка IQOS Iluma, но более компактен в использовании. Заряда устройства хватает на 20 стиков подряд.',
      img: `${this.baseUrl}/3.jpg`,
      price: '8 000 ₽',
      color: 'серый, зелёный, голубой, бежевый, розовый.'
    },
  ]

}
