import { ViewportScroller } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  constructor(private router: Router,private route: ActivatedRoute,private viewportScroller: ViewportScroller) { }

  @ViewChild('widgetsContent', { read: ElementRef }) public widgetsContent!: ElementRef<any>;

  boxes: any
  opacity: any
  ngOnInit() {
    this.boxes = document.querySelectorAll('.box');
    window.addEventListener('scroll', this.checkBoxes.bind(this)); // Bind `this` to the checkBoxes function
    this.checkBoxes(); // Call checkBoxes once to initially check box visibility

    this.opacity = document.querySelectorAll('.opacity');
    window.addEventListener('scroll', this.checkBoxesOpacity.bind(this)); // Bind `this` to the checkBoxes function
    this.checkBoxesOpacity(); // Call checkBoxes once to initially check box visibility
  }

  public scrollRight(): void {
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 700), behavior: 'smooth' });
  }
  public scrollLeft(): void {
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft - 700), behavior: 'smooth' });
  }

  private getWidgetWidth(): number {
    // получаем первый блок и измеряем его ширину
    return this.widgetsContent.nativeElement.querySelector('.wrapper_feedback').offsetWidth;
  }

  public scrollRightM(): void {
    const widgetWidth = this.getWidgetWidth();
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + widgetWidth), behavior: 'smooth' });
  }
  public scrollLeftM(): void {
    const widgetWidth = this.getWidgetWidth();
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft - widgetWidth), behavior: 'smooth' });
  }

  checkBoxes() {
    const triggerBottom = (window.innerHeight / 5) * 4;
    
    this.boxes.forEach((box: any) => {
      const boxTop = box.getBoundingClientRect().top;
    
      if (boxTop < triggerBottom) {
        box.classList.add('show');
      } else {
        box.classList.remove('show');
      }
    });
  }

  checkBoxesOpacity() {
    const triggerBottom = (window.innerHeight / 5) * 4;
    
    this.opacity.forEach((box: any) => {
      const boxTop = box.getBoundingClientRect().top;
    
      if (boxTop < triggerBottom) {
        box.style.opacity = '1';
      } else {
        box.style.opacity = '0';
      }
    });
  }

  title: string = 'Стики TEREA ВСЕГО от 3500 ₽ за блок'
  discrtiption1: string = 'Стики производства Кз , Армении, Индонезии и Европы'
  discrtiption2: string = 'Всего от 3500 ₽'

  aboutParagraph1: string = 'Добро пожаловать в наш магазин, ваш источник стиков Terea для IQOS! У нас вы найдете разнообразие продукции, включая популярные стики Terea и устройства, такие как iqos Iluma и iluma prime. Мы гордимся предлагаемым ассортиментом, который доступен не только в Москве, но и по всей России.'
  aboutParagraph2: string = 'Благодаря удобной доставке вы можете наслаждаться вашими любимыми стиками Terea в кратчайшие сроки. Не упустите шанс заказать качественные стики Terea прямо сейчас и получить удовольствие от вашего IQOS уже сегодня!'

  info: string = 'Откройте для себя широкий спектр вариантов и найдите идеальный вкус, который подчеркнет ваш индивидуальный стиль и предпочтения. Мы постоянно обновляем наш ассортимент, чтобы удовлетворить самые взыскательные вкусы наших клиентов. У нас в наличии широкий выбор вкусов стиков Terea для IQOS! От классических вариантов до уникальных иллюминирующих ароматов - найдите идеальный вкус для вашего IQOS уже сегодня!'
  subInfo: string = 'Наши услуги доставки являются самыми быстрыми в Москве. Мы предлагаем различные варианты доставки: от личного курьера до популярных служб доставки, таких как Яндекс.Доставка и СДЭК. Кроме того, у нас есть удобная опция самовывоза, которая позволяет вам получить ваш заказ и оплатить его на месте.'

  baseUrl: string = '../../../assets'
  assort: any = [
    {
      id: 1,
      name: 'IQOS Iluma standart',
      discrtiption: 'Самая консервативная модель в линейке. Она сохранила тот же дизайн, что и у IQOS DUO, зато начинка и функционал – такие же как у Iqos Iluma Prime',
      img: `${this.baseUrl}/1.jpg`,
    },
    {
      id: 2,
      name: 'IQOS Iluma Prime',
      discrtiption: 'Преимущества IQOS Iluma Prime по сравнению с другими моделями IQOS больше не будет неприятной проблемы, когда лезвие плохо входит в стик, так как лезвия отсутствует.',
      img: `${this.baseUrl}/2.jpg`,
    },
    {
      id: 3,
      name: 'IQOS Iluma ONE',
      discrtiption: 'Уникальная система нагрева табака, которая удивит вас своей простотой и функциональностью. ',
      img: `${this.baseUrl}/3.jpg`,
    },
  ]

  setId?: number;

  isShowDetails(id: number) {
    this.setId = id 
    if(id) { 
      this.router.navigate(['/details', id]); 
    } 
  }

  eu: any = [
    {
      id: 1,
      name: 'Warm fuse',
      discrtiption: 'Сочетание легкого табака с тонкой смесью тропических фруктов без ментола.',
      img: `${this.baseUrl}/e1.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 2,
      name: 'Amelia pearl',
      discrtiption: 'Сбалансированная смесь обжаренных табаков придает легкий пикантный травяной аромат. Один щелчок освобождает дополнительные освежающие и экзотические ноты.',
      img: `${this.baseUrl}/e2.jpg`,
      price: '7 900₽ блок',
      info: 'Стики имеют капсулу!',
      region: 'Производство: Европа'
    },
    {
      id: 3,
      name: 'Abora pearl',
      discrtiption: 'Вкус характеризуется лёгким и приятным ментолом, а также цитрусовым шлейфом с несколькими травяными оттенками.',
      img: `${this.baseUrl}/e3.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 4,
      name: 'Russet',
      discrtiption: 'Самый крепкий вкус. Богатая смесь обжаренных табаков, с приятными солодовыми ароматическими нотками.',
      img: `${this.baseUrl}/e4.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 5,
      name: 'Briza pearl',
      discrtiption: 'Обжаренный табачный бленд с древесными нотами и ароматом чая, одним нажатием вы можете добавить освежающий ментол.',
      img: `${this.baseUrl}/e5.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
    {
      id: 6,
      name: 'Soft Fuse',
      discrtiption: 'Сочетание легкого табака с сливочным послевкусием',
      img: `${this.baseUrl}/e6.jpg`,
      price: '7 900₽ блок',
      info: '',
      region: 'Производство: Европа'
    },
  ]

  id: any = [
    {
      id: 1,
      name: 'Green',
      discrtiption: 'Имеет освежающий вкус, с нотами мяты и лимона. Также может быть замечен легкий оттенок дыма и некоторая сладость.',
      img: `${this.baseUrl}/i1.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Индонезия'
    },
    {
      id: 2,
      name: 'Black green',
      discrtiption: 'Ощущение ментола, с мягкой табачной смесью и нотками зеленых фруктов.',
      img: `${this.baseUrl}/i2.jpg`,
      price: '7 500₽ блок',
      info: 'Стики имеют капсулу!',
      region: 'Производство: Индонезия'
    },
    {
      id: 3,
      name: 'Blue',
      discrtiption: 'Обладает некоторой сладостью с нотами фруктов, ванили, карамели и немного ментола.',
      img: `${this.baseUrl}/i3.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Индонезия'
    },
    {
      id: 4,
      name: 'Dimensions Yugen',
      discrtiption: 'Изысканный табачный бленд, дополненный комбинацией фруктовых нот и цветочных ароматов с завершающим свежим аккордом.',
      img: `${this.baseUrl}/i4.jpg`,
      price: '7 500₽ блок',
      info: '',
      region: 'Производство: Индонезия'
    },
  ]
  am: any = [
    {
      id: 1,
      name: 'Yellow',
      discrtiption: 'Имеет свежий и легкий вкус, с легкими фруктовыми нотками. Он может быть идеальным выбором для тех, кто предпочитает более легкий вкус табака. Кроме того, некоторые пользователи отмечают легкие пряные оттенки, которые могут добавлять некоторую сложность и глубину вкусу.',
      img: `${this.baseUrl}/a1.jpg`,
      price: '4 900₽ блок',
      info: '',
      region: 'Производство: Армения'
    },
    {
      id: 2,
      name: 'Sienna',
      discrtiption: 'Хорошо сбалансированная смесь обжаренного табака с древесными нотками и тонким ароматом чая.',
      img: `${this.baseUrl}/a2.jpg`,
      price: '4 900₽ блок',
      info: '',
      region: 'Производство: Армения'
    },
  ]
  kz: any = [
    {
      id: 1,
      name: 'Turquoise',
      discrtiption: 'Сочетание охлаждающих ментоловых ноток, ярко выраженное среди мощной табачной основы. Вкус отличается бодрящим всплеском свежести.',
      img: `${this.baseUrl}/k1.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 2,
      name: 'Silver*',
      discrtiption: 'Имеет более мягкий и гладкий вкус с оттенками свежести и легкой сладости. Он может быть идеальным выбором для тех, кто ищет более мягкий вкус табака. Кроме того, некоторые пользователи описывают его как немного пряный, с легким ароматом трав и цветов.',
      img: `${this.baseUrl}/k2.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 3,
      name: 'Bronze',
      discrtiption: 'Изысканный вкус отборного табачного бленда, в ароматической симфонии которого присутствуют нотки какао с деликатным отголоском сухофруктов, звучащих в унисон с терпкостью табачного листа.',
      img: `${this.baseUrl}/k3.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 4,
      name: 'Purple Wave',
      discrtiption: 'Охлаждающая и ароматная табачная смесь для любителей ментола и лесных ягод.',
      img: `${this.baseUrl}/k4.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 5,
      name: 'Summer Wave',
      discrtiption: 'Стики, в которых сочетаются изысканный аромат экзотических фруктов и прохладная свежесть мяты. Этот уникальный вкус предоставляет неповторимый опыт, объединяя сладость экзотических фруктов с освежающими нотками мяты.',
      img: `${this.baseUrl}/k5.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 6,
      name: 'Amber',
      discrtiption: 'Terea Amber описывается как насыщенный и глубокий, с легкой сладостью и пряными нотками. Он обладает терпким вкусом, который напоминает аромат традиционного табака, но при этом менее интенсивен и более мягок.',
      img: `${this.baseUrl}/k6.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 7,
      name: 'Ruby Fuse TEREA',
      discrtiption: 'Aроматный табачный вкус с нотками красных ягод и цветочными оттенками без ментола в яркой малиновой упаковке',
      img: `${this.baseUrl}/k7.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
    {
      id: 8,
      name: 'Green Zing',
      discrtiption: 'Если вы ищете что-то новое и необычное в мире табачных смесей, то цитрусовый и свежий табачный бленд - это именно то, что вам нужно! Этот уникальный продукт обладает удивительным сочетанием освежающего ментола, цитрусовых нот и тонких травяных оттенков.',
      img: `${this.baseUrl}/k8.jpg`,
      price: '3 500₽ блок',
      info: '',
      region: 'Производство: Казахстан'
    },
  ]
}

