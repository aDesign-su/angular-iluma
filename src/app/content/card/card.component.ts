import { Component, OnInit } from '@angular/core';
import { CountService } from '../general/count.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  constructor(public count:CountService,private router: Router) { }

  ngOnInit() {
    this.count.getCard()
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd && event.url === '/card') {
        window.location.reload();
      }
    });
  }
}
