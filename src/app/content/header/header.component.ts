import { Component, OnInit } from '@angular/core';
import { CountService } from '../general/count.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public count:CountService,private router: Router) {}

  ngOnInit() {

  }
  isCollapse: boolean = false

  isOpen() {
    this.isCollapse = !this.isCollapse
  }
  isClose() {
    this.isCollapse = false
  }
}
