import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { GeneralComponent } from './content/general/general.component';
import { HeaderComponent } from './content/header/header.component';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { FooterComponent } from './content/footer/footer.component';
import { DetailsComponent } from './content/general/details/details.component';
import { SticksComponent } from './content/sticks/sticks.component';
import { DevicesComponent } from './content/devices/devices.component';
import { AboutComponent } from './content/about/about.component';
import { CategoryComponent } from './content/general/category/category.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { CardComponent } from './content/card/card.component';
import { ACarouselComponent } from './content/general/a-carousel/a-carousel.component';
import { BCarouselComponent } from './content/general/b-carousel/b-carousel.component';
import { CCarouselComponent } from './content/general/c-carousel/c-carousel.component';
import { DCarouselComponent } from './content/general/d-carousel/d-carousel.component';
import { EuComponent } from './content/sticks/eu/eu.component';
import { IdComponent } from './content/sticks/id/id.component';
import { AmComponent } from './content/sticks/am/am.component';
import { KzComponent } from './content/sticks/kz/kz.component';

const appRoutes: Routes = [
  { path: '', component: GeneralComponent },
  { path: 'details/:id', component: DetailsComponent },
  { path: 'device', component: DevicesComponent },
  { path: 'sticks', component: SticksComponent },
  { path: 'about', component: AboutComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'card', component: CardComponent },
  { path: 'eu', component: EuComponent },
  { path: 'am', component: AmComponent },
  { path: 'kz', component: KzComponent },
  { path: 'id', component: IdComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent,
    DevicesComponent,
    SticksComponent,
    AboutComponent,
    GeneralComponent,
    DetailsComponent,
    CategoryComponent,
    HeaderComponent,
    FooterComponent,
    CardComponent,
    ACarouselComponent,
    BCarouselComponent,
    CCarouselComponent,
    DCarouselComponent,
    EuComponent,
    IdComponent,
    AmComponent,
    KzComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes,{enableTracing: false, anchorScrolling: 'enabled'}),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatGridListModule,
    MatCardModule,
    MatIconModule,
    MatDividerModule,
    MatExpansionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
